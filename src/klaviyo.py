import logging
import sys
import requests
import time
import json
import pandas as pd
import copy

CURRENT_TIMESTAMP = int(time.time())
# Default Table Output Destination
DEFAULT_TABLE_SOURCE = "/data/in/tables/"
DEFAULT_TABLE_DESTINATION = "/data/out/tables/"
DEFAULT_FILE_DESTINATION = "/data/out/files/"
DEFAULT_FILE_SOURCE = "/data/in/files/"


class Klaviyo():

    def __init__(self, api_token):
        self.api_token = api_token

    def process(self, endpoint, input_table_path, input_table_name, list_id=None):

        processed_counter = 0
        for chunk in pd.read_csv(input_table_path, chunksize=1000, dtype=str):

            if endpoint == 'add_update_profile':
                request_log = self.add_update_profile(
                    data_in=chunk,
                    input_table_name=input_table_name)

            elif endpoint == 'add_profiles_to_list':
                request_log = self.add_profiles_to_list(
                    data_in=chunk,
                    input_table_name=input_table_name,
                    list_id=list_id,
                    list_type='members')

            elif endpoint == 'subscribe_profiles_to_list':
                request_log = self.add_profiles_to_list(
                    data_in=chunk,
                    input_table_name=input_table_name,
                    list_id=list_id,
                    list_type='subscribe')

            # Output Request Log
            request_log_df = pd.DataFrame(request_log)
            request_log_df.to_csv(
                DEFAULT_TABLE_DESTINATION+'log.csv', index=False, mode='a', header=False)

            # Processor counter
            processed_counter += len(chunk.index)
            logging.info(f'Processed {processed_counter} records')

        manifest = {
            'incremental': True,
            'primary_key': [
                'timestamp',
                'request_url',
                'request_data'
            ],
            'columns': [
                'timestamp',
                'request_url',
                'request_data',
                'request_status',
                'request_response'
            ]
        }
        with open(DEFAULT_TABLE_DESTINATION+'log.csv.manifest', 'w') as f:
            json.dump(manifest, f)

    def add_update_profile(self, data_in, input_table_name):

        col_headers = list(data_in.columns)
        # Klaviyo default properties
        # required to alter the properties name
        klaviyo_default_properties = [
            'email',
            'first_name',
            'last_name',
            'phone_number',
            'title',
            'organization',
            'city',
            'region',
            'country',
            'zip',
            'image'
        ]
        # Request log
        request_log = []

        if 'id' not in col_headers:
            logging.error(f'Table [{input_table_name}] is missing required column: [id]')
            sys.exit(1)

        for index, row in data_in.iterrows():

            request_data = {
                'api_key': self.api_token
            }
            request_url = f'https://a.klaviyo.com/api/v1/person/{row["id"]}'

            for column in col_headers:

                if column == 'id':
                    continue

                param_name = f'${column}' if column in klaviyo_default_properties else column
                param_value = row[column]

                if not pd.isnull(param_value) and param_value != '':
                    request_data[param_name] = param_value

            # Data Request
            request_data_cloned = copy.deepcopy(request_data)
            del request_data_cloned['api_key']
            logging.debug(f'Posting: {request_data_cloned}')
            r = requests.put(request_url, data=request_data)

            # Outputting request log
            del request_data['api_key']
            log = self.construct_log(
                r, request_url, request_data)
            request_log.append(log)

        return request_log

    def add_profiles_to_list(self, data_in, input_table_name, list_id, list_type):
        col_headers = list(data_in.columns)

        # Input mapping validation
        # Ensuring all the required columns are included
        if 'email' not in col_headers or 'phone_number' not in col_headers:
            logging.error(
                f'Table {input_table_name} is missing one of [email] or [phone_numbe] key.')
            sys.exit(1)

        # Request Parameters
        request_log = []
        request_data = {
            'api_key': self.api_token,
            'profiles': []
        }
        request_headers = {
            'Content-Type': 'application/json'
        }
        request_url = f'https://a.klaviyo.com/api/v2/list/{list_id}/{list_type}'

        for index, row in data_in.iterrows():

            tmp_data = {}
            consent_validation = True

            for column in col_headers:

                param_name = column
                param_value = row[column]

                if not pd.isnull(param_value) and param_value != '':

                    # Re-configuring the variable 'consent'
                    if list_type == 'subscribe' and param_name == 'consent':
                        consent_values = ['email', 'web',
                                          'sms', 'directmail', 'mobile']

                        if param_value not in consent_values:
                            logging.debug(
                                'Entered consent value is not supported.')
                            logging.debug(
                                'Updating consent variable as a custom variable.')
                            consent_validation = False

                        else:
                            param_name = '$consent'

                    tmp_data[param_name] = param_value

            request_data['profiles'] = [tmp_data]

            # Data Request
            logging.debug(f'Posting: {request_data["profiles"][0]}')
            r = requests.post(request_url, json=request_data,
                              headers=request_headers)

            # Outputting request log
            request_data_log = request_data['profiles'][0]
            log = self.construct_log(
                r, request_url, request_data_log, consent_validation)
            request_log.append(log)

        return request_log

    def construct_log(self, request_response, request_url, request_data, consent=None):

        request_data_cloned = copy.deepcopy(request_data)
        # del request_data_cloned['api_key']

        log = {
            'timestamp': CURRENT_TIMESTAMP,
            'request_url': request_url,
            'request_data': request_data_cloned,
            'request_status': request_response.status_code,
            'request_response': ''
        }

        if consent is False and request_response.status_code == 200:
            consent_msg = 'WARNING: Entered consent value is not supported.'\
                'Updating consent variable as a custom variable.'
            log['request_response'] = consent_msg

        elif request_response.status_code != 200:
            log['request_response'] = request_response.text

        return log
