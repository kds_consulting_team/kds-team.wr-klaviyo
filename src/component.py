'''
Template Component main class.

'''

import logging
import sys
import os  # noqa
import datetime  # noqa
import requests

from kbc.env_handler import KBCEnvHandler
from kbc.result import KBCTableDef  # noqa
from kbc.result import ResultWriter  # noqa

from klaviyo import Klaviyo


# configuration variables
KEY_DEBUG = 'debug'
KEY_API_TOKEN = '#api_token'
KEY_ENDPOINT = 'endpoint'
KEY_LIST_ID = 'list_id'

MANDATORY_PARS = [
    KEY_ENDPOINT,
    KEY_API_TOKEN,
    KEY_LIST_ID
]
MANDATORY_IMAGE_PARS = []

APP_VERSION = '0.0.2'


class Component(KBCEnvHandler):

    def __init__(self, debug=False):
        KBCEnvHandler.__init__(self, MANDATORY_PARS)
        logging.info('Running version %s', APP_VERSION)
        logging.info('Loading configuration...')

        # Disabling list of libraries you want to output in the logger
        disable_libraries = []
        for library in disable_libraries:
            logging.getLogger(library).disabled = True

        # override debug from config
        if self.cfg_params.get(KEY_DEBUG):
            debug = True

        log_level = logging.DEBUG if debug else logging.INFO
        # setup GELF if available
        if os.getenv('KBC_LOGGER_ADDR', None):
            self.set_gelf_logger(log_level)
        else:
            self.set_default_logger(log_level)

        try:
            self.validate_config()
            self.validate_image_parameters(MANDATORY_IMAGE_PARS)
        except ValueError as e:
            logging.error(e)
            exit(1)

    def get_tables(self, tables, mapping):
        """
        Evaluate input and output table names.
        Only taking the first one into consideration!
        mapping: input_mapping, output_mappings
        """
        # input file
        table_list = []
        for table in tables:

            if mapping == "input_mapping":
                destination = table["destination"]
            elif mapping == "output_mapping":
                destination = table["source"]
            table_list.append(destination)

        return table_list

    def run(self):
        '''
        Main execution code
        '''
        # Get proper list of tables
        in_tables = self.configuration.get_input_tables()
        out_tables = self.configuration.get_expected_output_tables()
        in_table_names = self.get_tables(in_tables, 'input_mapping')
        out_table_names = self.get_tables(out_tables, 'output_mapping')
        logging.info("IN tables mapped: "+str(in_table_names))
        logging.info("OUT tables mapped: "+str(out_table_names))

        params = self.cfg_params  # noqa
        # Validating User inputs
        self.validate_user_input(params, in_tables)

        # Configuratio parameters
        api_token = params.get(KEY_API_TOKEN)
        endpoint = params.get(KEY_ENDPOINT)
        list_id = params.get(KEY_LIST_ID)

        # Klaviyo Class
        klaviyo = Klaviyo(api_token=api_token)

        for table in in_tables:

            logging.info(f'Processing [{table["destination"]}]')
            klaviyo.process(
                endpoint=endpoint,
                input_table_path=table['full_path'],
                input_table_name=table['destination'],
                list_id=list_id
            )

        logging.info("Klaviyo Writer finished")

    def validate_user_input(self, params, in_tables):
        # User Input Validation
        if not params:
            logging.error('Input configurations are missing.')
            sys.exit(1)

        else:
            if params[KEY_API_TOKEN] == '' or params[KEY_ENDPOINT] == '':
                logging.error(
                    'Required parameters are missing: [API token] & [Endpoint]')
                sys.exit(1)

            elif len(in_tables) == 0:
                logging.error('Input Mapping is missing.')
                sys.exit(1)

            elif params[KEY_ENDPOINT] in ['subscribe_profiles_to_list', 'add_profiles_to_list'] \
                    and params[KEY_LIST_ID] == '':
                logging.error('[List ID] is required.')
                sys.exit(1)

        # Testing input API token
        test_url = 'https://a.klaviyo.com/api/v2/lists'
        test_params = {
            'api_key': params[KEY_API_TOKEN]
        }
        r = requests.get(test_url, params=test_params)
        if r.status_code not in (200, 201):
            error_msg = r.json()['message']
            logging.error(error_msg)
            sys.exit(1)


"""
        Main entrypoint
"""
if __name__ == "__main__":
    if len(sys.argv) > 1:
        debug = sys.argv[1]
    else:
        debug = True
    comp = Component(debug)
    comp.run()
