## Parameters

1. API Token
    - Your Klaviyo Account API Token
    - To Fetch your API Token:
      1. Log into your Klaviyo platform
      2. Your profile name (top right)
      3. Account
      4. Settings
      5. API Keys
      6. Create API Key (if required)

2. Endpoint
    1. [Update Profile Attributes](https://www.klaviyo.com/docs/api/people)
        - Add or update one or more attirbutes ofr a person based on Person ID
        - If a property already exists, it will be updated
        - If a property is not set for that record, it will be created
        - To add a new profile into the Profile Pool, the profile will need to be added be using **Subscribe Profiles to List** or **Add Profiles to List**
        - Required column in input mapping
            1. id
        - Any properties not listed belong will be submitted as a custom property
            1. email
            2. first_name
            3. last_name
            4. phone_number
            5. title
            6. organization
            7. city
            8. region
            9. zip
            10. image

    2. [Subscribe Profiles to List](https://www.klaviyo.com/docs/api/v2/lists#post-subscribe)
        - Subscribe or re-subscribe profiles to a list
        - Request `must` contain `one of` properties below:
            1. email
            2. phone_number
        - If you are a GDPR compliant business, you will need to include `$consent`
            - Supported `$consent` value
                1. email
                2. web
                3. sms
                4. directmail
                5. mobile
            - If the cell value for `$consent` is none of the above, the respective consent column and value will be submitted as a custom property

    3. [Add Profiles to List](https://www.klaviyo.com/docs/api/v2/lists#post-members)
        - Add profiles to a list
        - This endpoint is functionally equivalent to adding profiles to a list via a CSV upload and will immediately add profiles to the list
        - Request `must` contain `one of` properties below:
            1. email
            2. phone_number